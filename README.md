# FoodTracker (work in progress)

I started running through this [iOS tutorial](https://developer.apple.com/library/ios/referencelibrary/GettingStarted/DevelopiOSAppsSwift/index.html). The goal is to create a Swift app that lets the user add (food) items, upload images, and then rate them. I did this as part of my research into the advantages of native mobile apps versus web apps (and web/mobile-hybrid apps using e.g. Cordova). I have put the project on hold so I can focus on my web developer skills.
